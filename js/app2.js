const dataset = 'data/clicks_json.json';

let colorScale = d3.scaleOrdinal(d3['schemePastel1']);

var margin = {top: 20, right: 20, bottom: 50, left: 70},
    width = 960 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

var x = d3.scaleTime().range([0, width]);
var y = d3.scaleLinear().range([height, 0]);

var svg = d3.select("svg#scatter-plot")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform",
        "translate(" + margin.left + "," + margin.top + ")");

let tooltip = d3.select("body").append("div").attr("class", "tooltip").style("opacity", 0);

d3.json(dataset, function (err, data) {
    if(err){console.warn("Error! Data is undefined")}

    let clientsArr = getDistinctClients(data);

    data.forEach(function(d) {
        d.xScale = d.ox;
        d.yScale = d.oy;
    });

    x.domain(d3.extent(data, function(d) { return d.xScale; }));
    y.domain([0, d3.max(data, function(d) { return d.yScale; })]);

    svg.selectAll("dot")
        .data(data)
        .enter()
        .append("circle")
        .attr("r", 5)
        .attr("cx", function (d) {
        return x(d.ox);
    })
        .attr("cy", function (d) {
        return y(d.oy);
    })
        .attr("fill", function (d, i) {
        return d.is_fraud !== 1 ? colorScale(d.client) : "#ff0001";
    }).attr("opacity", .6)
        .on('mouseover', function (d) {
            this.style.stroke = "#000000";
            tooltip.transition()
                .duration(200)
                .style("opacity", .9);
            tooltip.html(
                "<strong>Client:</strong> " + d.client + "<br/>"
                + "<strong>Is Fraud:</strong> " + ((d.is_fraud === 1) ? "Yes" : "No") + "<br/>"
                + "<strong>Moving:</strong> " + d.Moving + "<br/>"
                + "<strong>Left Dragging:</strong> " + d.LeftDragging + "<br/>"
                + "<strong>Right Dragging:</strong> " + d.RightDragging + "<br/>"
                + "<strong>Left Clicking:</strong> " + d.LeftClicking + "<br/>"
                + "<strong>Right Clicking:</strong> " + d.RightClicking + "<br/>"
                + "<strong>Scrolling Up:</strong> " + d.ScrollingUp + "<br/>"
                + "<strong>Scrolling Down:</strong> " + d.ScrollingDown + "<br/>"
                + "<strong>Duration Client:</strong> " + d.duration_client + "<br/>"
                + "<strong>Duration Record:</strong> " + d.duration_record + "<br/>"
                + "<strong>Distance Traveled:</strong> " + d.distance_traveled + "<br/>"
            )
                .style("left", (d3.event.pageX) + 10 + "px")
                .style("top", (d3.event.pageY - 28) + "px");
    })
        .on('mouseout', function (d) {
            this.style.stroke = "";
            tooltip.transition()
                .duration(500)
                .style("opacity", 0);
    });
});

let getDistinctClients = (arr) => {
    let flags =[], output = [], l = arr.length, i;
    for(i = 0; i < l; i++) {
        if(flags[arr[i].client]) {continue;}
        flags[arr[i].client] = true;
        output.push(arr[i].client);
    }
    return output;
};

